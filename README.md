# SOLUTION

Hello! Thanks for reviewing this code.

To start the server just type `docker-compose up` (considering you have docker installed)

To "migrate" the database, just run `bash migrate_db.bash`. It will create the database and the tables as well.
Also, 3 users will be added to the database:

| Username       | Password    | Type       |
| --------       | --------    | ---------- |
| bill.gates     | Bill        | technician |
| steve.jobs     | Steve       | manager    |
| erlich.bachman | Avocado     | technician |

Alternatevely, you can add a new user by sending a post to `/api/users` like the following:
```
curl -XPOST -H "Content-type: application/json" -d '{
    "username": "bertram.gilfoyle",
    "password": "Anton",
    "type": "technician"
}' 'http://localhost/api/users'
```

Now in order to start testing the API, you have to authenticate a technician user and add a new task. But first, let's login by using the route `/api/users/login`:
```
curl -XPOST -H "Content-type: application/json" -d '{
    "username": "bertram.gilfoyle",
    "password": "Anton"
}' 'http://localhost/api/users/login'
```
This will return the token need to access private areas of the API. Get the token from the request result and now make a request to create a new task with the header `Authorization` and the value `Bearer <token>` where `token` is the result from last request: `/api/tasks`:
```
curl -XPOST -H 'Authorization: Bearer <token>' -H "Content-type: application/json" -d '{
    "summary": "Build a chatbot to answer Dinesh on Slack."
}' 'http://localhost/api/tasks'
```
This request will return the task ID. Now you can use it to get and update your task. Let's get it first at the route `/api/tasks/<id>`:
```
curl -XGET -H 'Authorization: Bearer <token>' 'http://localhost/api/tasks/1'
```
Great, now we can complete this task. The simplest way is to send a POST to `/tasks/1/complete`:
```
curl -XPOST -H 'Authorization: Bearer <token>' 'http://localhost/api/tasks/1/complete'
```
The other way is updating it. I will mark it as complete by passing the date it was completed. To update we will use the PUT route `/api/tasks/<id>`. 
PS: (for the sake of simplicity, you have to provide the summary as well.)
```
curl -XPUT -H 'Authorization: Bearer <token>' -H "Content-type: application/json" -d '{
    "summary": "Build a chatbot to answer Dinesh on Slack.",
    "completed_at": "2021-01-01 00:00:00"
}' 'http://localhost/api/tasks/1'
```
If you check the console where docker is running, the "php_receiver" will print a message telling that the user has finished a task.
Ok. That is everything a technician can do (besides getting all his tasks). Now we need to login as a manager to proceed. After you grab the token, execute the following request `/api/tasks`:
```
curl -XGET -H 'Authorization: Bearer <token>' 'http://localhost/api/tasks'
```
This will list all the tasks. You can now delete a task that is no longer needed in the route `/api/tasks/<id>`:
```
curl -XDELETE -H 'Authorization: Bearer <token>' 'http://localhost/api/tasks/1'
```

----
## Development

As this is a simple API, I have chosen Slim4 for the sake of simplicity and testability. If I had used Laravel or Symfony it will be an overkill and doing it "from scratch" would be less time effective (authentication, etc).

For the authentication I used the simplest JWT validation that I could find. Obviously this is not "production" material since the JWT is not checked for expiration (for example). The same goes for the encryption of the summary, there could be a salt as well.

The kubernetes object files were created by `kompose convert`.

I am available for any doubts or if something does not work well.

Tests can be run by running:
`
./vendor/bin/phpunit
`

# SWORD HEALTH test

## API developer practical exercise

### Requirements
You are developing a software to account for maintenance tasks performed during a working day. This application
has two types of users (Manager, Technician).
The technician performs tasks and is only able to see, create or update his own performed tasks.
The manager can see tasks from all the technicians, delete them, and should be notified when some tech performs
a task.
A task has a summary (max: 2500 characters) and a date when it was performed, the summary from the task can
contain personal information.

#### Notes:
- If you don’t have enough time to complete the test you should prioritize complete features ( with tests ) over
many features.
- We’ll evaluate security, quality and readability of your code
- This test is suitable for all levels of developers, so make sure to prove yours

### Development

#### Features:
- Create API endpoint to save a new task
- Create API endpoint to list tasks
- Notify manager of each task performed by the tech (This notification can be just a print saying “The tech X
performed the task Y on date Z”)
- This notification should not block any http request

#### Tech Requirements:
- Use any language to develop this HTTP API (we use Go, Node and PHP)
- Create a local development environment using docker containing this service and a MySQL database
- Use MySQL database to persist data from the application
- Features should have unit tests to ensure they are working properly

#### Bonus
- Use a message broker to decouple notification logic from the application flow
- Create Kubernetes object files needed to deploy this application