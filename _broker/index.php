<?php

require_once __DIR__ . '/../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\Dotenv\Dotenv;



$dotEnv = new Dotenv();
$dotEnv->load(__DIR__ . '/../.env');

// just to wait rabbitmq to start
sleep(10);

$connection = new AMQPStreamConnection(
    $_ENV['MESSAGE_BROKER_HOST'],
    $_ENV['MESSAGE_BROKER_PORT'],
    $_ENV['MESSAGE_BROKER_USER'],
    $_ENV['MESSAGE_BROKER_PASSWORD']
);
$channel = $connection->channel();

$queue = $_ENV['MESSAGE_BROKER_QUEUE'];
$channel->queue_declare($queue, false, true, false, false);

echo  $queue,' Queue started. Waiting for messages', "\n";

$callback = function ($msg) {
    $waitSeconds = rand(15, 30);
    echo "\n\nMessage Received: ", $msg->body, "\n\n\n";
    sleep($waitSeconds);
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume($queue, '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}