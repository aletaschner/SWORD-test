<?php

declare(strict_types=1);

namespace Tests\Infrastructure;

use App\Infrastructure\MessageBroker;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Tests\TestCase;

class MessageBrokerTest extends TestCase
{
    use ProphecyTrait;

    public function testMessageIsSent()
    {
        $messageBroker = new MessageBroker('test');
        $connection = $this->createMock(AMQPStreamConnection::class);

        $channel = $this->prophesize(AMQPChannel::class);
        $channel->queue_declare('test', false, true, false, false)->shouldBeCalledTimes(1);
        $channel->basic_publish(Argument::type(AMQPMessage::class), '', 'test')->shouldBeCalledTimes(1);
        $channel->close()->shouldBeCalledTimes(1);

        $connection->method('channel')->willReturn($channel->reveal());
        $messageBroker->setConnection($connection);

        $messageBroker->notify('this is a test.');
    }
}
