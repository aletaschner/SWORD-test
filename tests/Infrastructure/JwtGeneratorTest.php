<?php
declare(strict_types=1);

namespace Tests\Infrastructure;

use App\Domain\User\User;
use Tests\TestCase;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use App\Infrastructure\JwtGenerator;

class JwtGeneratorTest extends TestCase
{


    public function userProvider()
    {
        return [
            [1, 'bill.gates', 'Bill', User::USER_TYPE_TECHNICIAN],
            [2, 'steve.jobs', 'Steve', User::USER_TYPE_MANAGER],
            [3, 'mark.zuckerberg', 'Mark', User::USER_TYPE_TECHNICIAN],
            [4, 'evan.spiegel', 'Evan', User::USER_TYPE_MANAGER],
            [5, 'jack.dorsey', 'Jack', User::USER_TYPE_TECHNICIAN],
        ];
    }    

    /**
     * @dataProvider userProvider
     * @param int    $id
     * @param string $username
     * @param string $password
     * @param string $type
     */
    public function testGenerationIsCorrect(int $id, string $username, string $password, string $type)
    {
        $jwtGenerator = new JwtGenerator('test-secret', 'HS256');
        $user = new User($id, $username, $password, $type);

        $jwt = $jwtGenerator->generate($user->jsonSerialize());

        $result = (array) JWT::decode($jwt, 'test-secret', ['HS256']);

        $this->assertEquals($user->jsonSerialize(), $result);
    }
    
    /**
     * @dataProvider userProvider
     * @param int    $id
     * @param string $username
     * @param string $password
     * @param string $type
     */
    public function testWrongSecretFails(int $id, string $username, string $password, string $type)
    {
        $jwtGenerator = new JwtGenerator('test-secret', 'HS256');
        $user = new User($id, $username, $password, $type);

        $jwt = $jwtGenerator->generate($user->jsonSerialize());

        $this->expectException(SignatureInvalidException::class);
        $result = (array) JWT::decode($jwt, 'test-secret2', ['HS256']);
    }
}
