<?php
declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Task;

use App\Domain\Task\Task;
use App\Domain\User\User;
use App\Domain\Task\TaskNotFoundException;
use App\Infrastructure\Persistence\Task\DatabaseTaskRepository;
use App\Infrastructure\DataEncryptor;
use Tests\TestCase;

class DatabaseTaskRepositoryTest extends TestCase
{
    public function testSummaryIsDecripted()
    {
        $task = ['id'=> 1, 'user_id'=> 1, 'summary' => '2W4SFhctV87VVnrn+QG7Mw=='];
        $pdo = $this->createMock(\PDO::class);
        $stm = $this->createMock(\PDOStatement::class);
        $pdo->method('prepare')->willReturn($stm);
        $stm->method('fetch')->willReturn($task);
        $stm->method('fetchAll')->willReturn([$task]);

        $dataEncryptor = new DataEncryptor('key', 'aes-256-cbc', 'VuIxSKQMkANvvCplvRAu2A==');
        $repository = new DatabaseTaskRepository($pdo, $dataEncryptor);
        $taskFromDatabase = $repository->findTaskById(1);
        $this->assertEquals('bill.gates', $taskFromDatabase->getSummary());
        
        $taskFromDatabase = $repository->findTasksAllowedForUser(1, User::USER_TYPE_TECHNICIAN)[0];
        $this->assertEquals('bill.gates', $taskFromDatabase->getSummary());
        
    }

    public function testTaskNotFoundWillThrowException()
    {
        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn(new \PDOStatement());
        $stm = $this->createMock(\PDOStatement::class);
        $pdo->method('prepare')->willReturn($stm);
        $stm->method('fetch')->willReturn(false);
        
        $dataEncryptor = new DataEncryptor('key', 'aes-256-cbc', 'VuIxSKQMkANvvCplvRAu2A==');
        $repository = new DatabaseTaskRepository($pdo, $dataEncryptor);

        $this->expectException(TaskNotFoundException::class);
        $userFromDatabase = $repository->findTaskById(1);
    }
}
