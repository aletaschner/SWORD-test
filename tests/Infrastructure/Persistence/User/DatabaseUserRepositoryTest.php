<?php

declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Infrastructure\Persistence\User\DatabaseUserRepository;
use App\Infrastructure\DataEncryptor;
use Tests\TestCase;

class DatabaseUserRepositoryTest extends TestCase
{
    public function testDatabaseIsCorrectlyCalled()
    {
        $password = 'bill';
        $user = [
            'id' => 1,
            'username' => 'bill.gates',
            'password' => password_hash($password, PASSWORD_BCRYPT),
            'type' => User::USER_TYPE_TECHNICIAN,
        ];
        $pdo = $this->createMock(\PDO::class);
        $stm = $this->createMock(\PDOStatement::class);
        $pdo->method('prepare')->willReturn($stm);
        $stm->method('fetch')->willReturn($user);

        $repository = new DatabaseUserRepository($pdo, $this->createMock(DataEncryptor::class));
        $userFromDatabase = $repository->findUserOfUsername('bill.gates');

        $this->assertEquals($user['id'], $userFromDatabase->getId());
        $this->assertEquals($user['username'], $userFromDatabase->getUsername());
        $this->assertEquals($user['type'], $userFromDatabase->getType());
        $this->assertTrue($userFromDatabase->isPasswordCorrect($password));
    }

    public function testUserNotFoundWillThrowException()
    {
        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn(new \PDOStatement());
        $pdo->method('query')->willReturn(false);

        $repository = new DatabaseUserRepository($pdo, $this->createMock(DataEncryptor::class));

        $this->expectException(UserNotFoundException::class);
        $userFromDatabase = $repository->findUserOfUsername('steve.jobs');
    }

    public function testAdd()
    {
        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn(new \PDOStatement());
        $pdo->method('lastInsertId')->willReturn(1);

        $repository = new DatabaseUserRepository($pdo, $this->createMock(DataEncryptor::class));

        $addedUserId = $repository->addUser(new User(1, '1', '1', User::USER_TYPE_TECHNICIAN));
        $this->assertEquals(1, $addedUserId);
    }
}
