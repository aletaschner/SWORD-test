<?php
declare(strict_types=1);

namespace Tests\Infrastructure;

use App\Domain\User\User;
use Tests\TestCase;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use App\Infrastructure\DataEncryptor;

class DataEncryptorTest extends TestCase
{


    public function provider()
    {
        return [
            ['bill.gates', '2W4SFhctV87VVnrn+QG7Mw=='],
            ['steve.jobs', 'Rx4tlExGm7vJUmFCr5AV4g=='],
            ['mark.zuckerberg', '1fquediws/Hb3fZ97Ryixg=='],
            ['evan.spiegel', 'sLNPwJk6fzpngvztaetjKw=='],
            ['jack.dorsey', '7QMcZa2AwFl1legnKWpEUg=='],
        ];
    }    

    /**
     * @dataProvider provider
     * @param int    $id
     * @param string $username
     * @param string $password
     * @param string $type
     */
    public function testEncryptionIsCorrect($actual, $expected)
    {
        $dataEncryptor = new DataEncryptor('key', 'aes-256-cbc', 'VuIxSKQMkANvvCplvRAu2A==');
        $encrypted = $dataEncryptor->encrypt($actual);
        $this->assertEquals($expected, $encrypted);
    }
    
    /**
     * @dataProvider provider
     * @param string $username
     * @param string $password
     */
    public function testDecryptionIsCorrect($expected, $actual)
    {
        $dataEncryptor = new DataEncryptor('key', 'aes-256-cbc', 'VuIxSKQMkANvvCplvRAu2A==');
        $decrypted = $dataEncryptor->decrypt($actual);
        $this->assertEquals($expected, $decrypted);
    }
}
