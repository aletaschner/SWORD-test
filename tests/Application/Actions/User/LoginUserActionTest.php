<?php
declare(strict_types=1);

namespace Tests\Application\Actions\User;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use DI\Container;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;
use App\Infrastructure\JwtGenerator;

class LoginUserActionTest extends TestCase
{
    public function testActionWorksProperly()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $user = new User(1, 'bill.gates', 'Bill', User::USER_TYPE_TECHNICIAN);

        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->method('findUserOfUsername')->willReturn($user);

        $container->set(UserRepository::class, $userRepositoryMock);
    
        $request = $this->createRequest('POST', '/api/users/login');
        $request->getBody()->write(json_encode(['username' => 'bill.gates', 'password' => 'Bill']));
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $jwtEncoder = $container->get(JwtGenerator::class);
        $expectedPayload = new ActionPayload(200, $jwtEncoder->generate($user->jsonSerialize()));
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionsThrowsUserNotFoundException()
    {
        $app = $this->getAppInstance();

        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false ,false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $app->add($errorMiddleware);

        /** @var Container $container */
        $container = $app->getContainer();

        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->method('findUserOfUsername')->willThrowException(new UserNotFoundException);
        $container->set(UserRepository::class, $userRepositoryMock);

        $request = $this->createRequest('POST', '/api/users/login');
        $request->getBody()->write(json_encode(['username' => '', 'password' => 'Bill']));
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(ActionError::RESOURCE_NOT_FOUND, 'The user you requested does not exist.');
        $expectedPayload = new ActionPayload(404, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionThrowsInvalidPasswordException()
    {
        $app = $this->getAppInstance();

        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false ,false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $app->add($errorMiddleware);

        /** @var Container $container */
        $container = $app->getContainer();

        $user = new User(1, 'bill.gates', 'Bill', User::USER_TYPE_TECHNICIAN);
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->method('findUserOfUsername')->willReturn($user);
        $container->set(UserRepository::class, $userRepositoryMock);

        $request = $this->createRequest('POST', '/api/users/login');
        $request->getBody()->write(\json_encode(['username' => 'bill.gates', 'password' => 'test']));
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(ActionError::RESOURCE_NOT_FOUND, 'The user you requested does not exist.');
        $expectedPayload = new ActionPayload(404, null, $expectedError);
        $serializedPayload = \json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
