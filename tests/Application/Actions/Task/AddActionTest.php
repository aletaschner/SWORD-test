<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Task;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\Task\TaskRepository;
use App\Domain\User\User;
use DI\Container;
use Psr\Http\Message\ResponseInterface;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;
use App\Infrastructure\JwtGenerator;

class AddActionTest extends TestCase
{
    /** @var \Slim\App $app */
    protected $app;
    /** @var Container $container */
    protected Container $container;
    /** @var User $user */
    protected User $user;
    /** @var JwtGenerator $jwtEncoder */
    protected JwtGenerator $jwtEncoder;


    protected function setUp(): void
    {
        $this->app = $this->getAppInstance();
        $this->container = $this->app->getContainer();
        $this->jwtEncoder = $this->container->get(JwtGenerator::class);
        $this->user = new User(1, 'test', 'test', User::USER_TYPE_TECHNICIAN);
    }

    /**
     * @param array $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function createAddRequest(array $body): ResponseInterface
    {
        $token = $this->jwtEncoder->generate($this->user->jsonSerialize());
        $request = $this->createRequest('POST', '/api/tasks')
            ->withHeader('Authorization', 'Bearer ' . $token);
        $request->getBody()->write(\json_encode($body));
        return $this->app->handle($request);
    }

    public function testActionWorksProperly()
    {
        $taskRepositoryMock = $this->createMock(TaskRepository::class);
        $taskRepositoryMock->method('addTask')->willReturn(1);
        $this->container->set(TaskRepository::class, $taskRepositoryMock);

        $response = $this->createAddRequest(['summary' => 'Develop a middle out compression algorithm.']);

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, '1');
        $serializedPayload = \json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionIsUnauthorizedForManager()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $taskRepositoryMock = $this->createMock(TaskRepository::class);
        $this->container->set(TaskRepository::class, $taskRepositoryMock);

        $this->user = new User(1, '1', '1', User::USER_TYPE_MANAGER);
        $response = $this->createAddRequest(['summary' => 'Develop a middle out compression algorithm.']);

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::UNAUTHENTICATED, 'You are not authorized to do this action.');
        $expectedPayload = new ActionPayload(401, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionSummariesBiggerThan2500CharactersFail()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $taskRepositoryMock = $this->createMock(TaskRepository::class);
        $this->container->set(TaskRepository::class, $taskRepositoryMock);

        $response = $this->createAddRequest(
            ['summary' => str_repeat('Develop a middle out compression algorithm.', 60)]
        );

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::BAD_REQUEST, 'The summary can not exceed 2500 characters.');
        $expectedPayload = new ActionPayload(400, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionSummaryIsNotInformed()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $taskRepositoryMock = $this->createMock(TaskRepository::class);
        $this->container->set(TaskRepository::class, $taskRepositoryMock);

        $response = $this->createAddRequest(
            ['summary' => '']
        );

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::BAD_REQUEST, 'You must provide a summary.');
        $expectedPayload = new ActionPayload(400, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
