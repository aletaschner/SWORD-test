<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Task;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\Task\Task;
use App\Domain\Task\TaskRepository;
use App\Domain\User\User;
use DI\Container;
use Psr\Http\Message\ResponseInterface;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;
use App\Infrastructure\JwtGenerator;

class RemoveActionTest extends TestCase
{
    /** @var \Slim\App $app */
    protected $app;
    /** @var Container $container */
    protected Container $container;
    /** @var User $user */
    protected User $user;
    /** @var JwtGenerator $jwtEncoder */
    protected JwtGenerator $jwtEncoder;


    protected function setUp(): void
    {
        $this->app = $this->getAppInstance();
        $this->container = $this->app->getContainer();
        $this->jwtEncoder = $this->container->get(JwtGenerator::class);
        $this->user = new User(1, 'test', 'test', User::USER_TYPE_MANAGER);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function createRemoveRequest(): ResponseInterface
    {
        $token = $this->jwtEncoder->generate($this->user->jsonSerialize());
        $request = $this->createRequest('DELETE', '/api/tasks/1')
            ->withHeader('Authorization', 'Bearer ' . $token);
        return $this->app->handle($request);
    }


    /**
     * @throws \PHPUnit\Framework\MockObject\IncompatibleReturnValueException
     */
    protected function prepareDefaultRepositoryMock()
    {
        $taskRepositoryMock = $this->createMock(TaskRepository::class);
        $task = new Task(1, 1, 'Summary');
        $taskRepositoryMock->method('findTaskById')->willReturn($task);
        $taskRepositoryMock->method('removeTask')->willReturn(true);
        $this->container->set(TaskRepository::class, $taskRepositoryMock);
    }

    public function testActionWorksProperly()
    {
        $this->prepareDefaultRepositoryMock();
        $response = $this->createRemoveRequest();

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, true);
        $serializedPayload = \json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionIsUnauthorizedForTechnician()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $this->prepareDefaultRepositoryMock();
        $this->user = new User(1, '1', '1', User::USER_TYPE_TECHNICIAN);
        $response = $this->createRemoveRequest();

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::UNAUTHENTICATED, 'You are not authorized to do this action.');
        $expectedPayload = new ActionPayload(401, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
