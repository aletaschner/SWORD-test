<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Task;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\Task\Task;
use App\Domain\Task\TaskRepository;
use App\Domain\User\User;
use App\Infrastructure\MessageBroker;
use DI\Container;
use Psr\Http\Message\ResponseInterface;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;
use App\Infrastructure\JwtGenerator;

class UpdateActionTest extends TestCase
{
    /** @var \Slim\App $app */
    protected $app;
    /** @var Container $container */
    protected Container $container;
    /** @var User $user */
    protected User $user;
    /** @var JwtGenerator $jwtEncoder */
    protected JwtGenerator $jwtEncoder;


    protected function setUp(): void
    {
        $this->app = $this->getAppInstance();
        $this->container = $this->app->getContainer();
        $this->jwtEncoder = $this->container->get(JwtGenerator::class);
        $this->user = new User(1, 'test', 'test', User::USER_TYPE_TECHNICIAN);
    }

    /**
     * @param array $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function createUpdateRequest(array $body): ResponseInterface
    {
        $token = $this->jwtEncoder->generate($this->user->jsonSerialize());
        $request = $this->createRequest('PUT', '/api/tasks/1')
            ->withHeader('Authorization', 'Bearer ' . $token);
        $request->getBody()->write(\json_encode($body));
        return $this->app->handle($request);
    }


    /**
     * @throws \PHPUnit\Framework\MockObject\IncompatibleReturnValueException
     */
    protected function prepareDefaultRepositoryMock()
    {
        $taskRepositoryMock = $this->createMock(TaskRepository::class);
        $task = new Task(1, 1, 'Summary');
        $taskRepositoryMock->method('findTaskById')->willReturn($task);
        $taskRepositoryMock->method('updateTask')->willReturn(true);
        $this->container->set(TaskRepository::class, $taskRepositoryMock);
    }

    public function testActionWorksProperly()
    {
        $this->prepareDefaultRepositoryMock();
        $response = $this->createUpdateRequest(['summary' => 'Develop a middle out compression algorithm.']);

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, true);
        $serializedPayload = \json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionWorksProperlyWhenUpdatingCompletion()
    {
        $this->prepareDefaultRepositoryMock();
        $this->container->set(MessageBroker::class, $this->createMock(MessageBroker::class));

        $response = $this->createUpdateRequest(
            ['summary' => 'Develop a middle out compression algorithm.', 'completed_at' => \date('Y-m-d H:i:s')]
        );

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, true);
        $serializedPayload = \json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionIsUnauthorizedForManager()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $this->prepareDefaultRepositoryMock();
        $this->user = new User(1, '1', '1', User::USER_TYPE_MANAGER);
        $response = $this->createUpdateRequest(['summary' => 'Develop a middle out compression algorithm.']);

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::UNAUTHENTICATED, 'You are not authorized to do this action.');
        $expectedPayload = new ActionPayload(401, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionSummariesBiggerThan2500CharactersFail()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $this->prepareDefaultRepositoryMock();
        $response = $this->createUpdateRequest(
            ['summary' => str_repeat('Develop a middle out compression algorithm.', 60)]
        );

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::BAD_REQUEST, 'The summary can not exceed 2500 characters.');
        $expectedPayload = new ActionPayload(400, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testActionSummaryIsNotInformed()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $this->prepareDefaultRepositoryMock();
        $response = $this->createUpdateRequest(
            ['summary' => '']
        );

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::BAD_REQUEST, 'You must provide a summary.');
        $expectedPayload = new ActionPayload(400, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testUserThatDoesNotOwnTaskUpdateFails()
    {
        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false, false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);
        $this->app->add($errorMiddleware);

        $this->prepareDefaultRepositoryMock();
        $this->user = new User(5, '1', '1', User::USER_TYPE_MANAGER);
        $response = $this->createUpdateRequest(['summary' => 'Develop a middle out compression algorithm.']);

        $payload = (string)$response->getBody();
        $expectedError = new ActionError(ActionError::UNAUTHENTICATED, 'You are not authorized to do this action.');
        $expectedPayload = new ActionPayload(401, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

}
