<?php
declare(strict_types=1);

namespace Tests\Domain\Task;

use App\Domain\Task\Task;
use Tests\TestCase;

class TaskTest extends TestCase
{
    public function taskProvider()
    {
        return [
            [1, 1, 'Create a folder'],
            [2, 1, 'Create file'],
            [3, 2, 'Write hello world'],
            [4, 2, 'Turn off computer', new \DateTime('2021-01-11 02:20:01')],
            [5, 3, 'Go to sleep', new \DateTime('2020-01-11 02:20:20')],
        ];
    }

    /**
     * @dataProvider taskProvider
     * @param int               $id
     * @param int               $userId
     * @param string            $summary
     * @param \DateTime|null    $completedAt
     */
    public function testGetters(int $id, int $userId, string $summary, \DateTime $completedAt = null)
    {
        $task = new Task($id, $userId, $summary, $completedAt);

        $this->assertEquals($id, $task->getId());
        $this->assertEquals($userId, $task->getUserId());
        $this->assertEquals($summary, $task->getSummary());
        $this->assertEquals($completedAt, $task->getCompletedAt());
    }
    
    /**
     * @dataProvider taskProvider
     * @param int               $id
     * @param int               $userId
     * @param string            $summary
     * @param \DateTime|null    $completedAt
     */
    public function testJsonSerialize(int $id, int $userId, string $summary, \DateTime $completedAt = null)
    {
        $task = new Task($id, $userId, $summary, $completedAt);

        $expectedPayload = json_encode([
            'id' => $id,
            'user_id' => $userId,
            'summary' => $summary,
            'completed_at' => ($completedAt) ? $completedAt->format(\DateTime::W3C) : null
        ]);

        $this->assertEquals($expectedPayload, json_encode($task));
    }

    public function testMarkingAsCompleteWorksProperly()
    {
        $task = new Task(1, 1, 'test');
        $this->assertNull($task->getCompletedAt());
        $this->assertInstanceOf(Task::class, $task->markAsCompleted());
        $this->assertInstanceOf(\DateTime::class, $task->getCompletedAt());
    }
}
