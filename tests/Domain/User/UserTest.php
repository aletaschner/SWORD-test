<?php
declare(strict_types=1);

namespace Tests\Domain\User;

use App\Domain\User\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function userProvider()
    {
        return [
            [1, 'bill.gates', 'Bill', User::USER_TYPE_TECHNICIAN],
            [2, 'steve.jobs', 'Steve', User::USER_TYPE_MANAGER],
            [3, 'mark.zuckerberg', 'Mark', User::USER_TYPE_TECHNICIAN],
            [4, 'evan.spiegel', 'Evan', User::USER_TYPE_MANAGER],
            [5, 'jack.dorsey', 'Jack', User::USER_TYPE_TECHNICIAN],
        ];
    }

    /**
     * 
     */
    public function testInvalidUserType()
    {
        $this->expectException(\Exception::class);
        $user = new User(null, 'cr7', 'Ronaldo', 'footballer');
    }

    /**
     * @dataProvider userProvider
     * @param int    $id
     * @param string $username
     * @param string $password
     * @param string $type
     */
    public function testPasswordsAreHashed(int $id, string $username, string $password, string $type)
    {

        $user = new User($id, $username, $password, $type);
        
        $this->assertTrue($user->isPasswordCorrect($password));
    }

    /**
     * @dataProvider userProvider
     * @param int    $id
     * @param string $username
     * @param string $password
     * @param string $type
     */
    public function testGetters(int $id, string $username, string $password, string $type)
    {
        $user = new User($id, $username, $password, $type);

        $this->assertEquals($id, $user->getId());
        $this->assertEquals($username, $user->getUsername());
        $this->assertEquals($type, $user->getType());
    }

    /**
     * @dataProvider userProvider
     * @param int    $id
     * @param string $username
     * @param string $password
     * @param string $type
     */
    public function testJsonSerialize(int $id, string $username, string $password, string $type)
    {
        $user = new User($id, $username, $password, $type);

        $expectedPayload = json_encode([
            'id' => $id,
            'username' => $username,
            'type' => $type,
        ]);

        $this->assertEquals($expectedPayload, json_encode($user));
        $this->assertFalse(strripos(json_encode($user), 'password'));
    }
}
