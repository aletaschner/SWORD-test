<?php
declare(strict_types=1);

use App\Application\Actions\Task\AddTaskAction;
use App\Application\Actions\Task\CompleteTaskAction;
use App\Application\Actions\Task\FindAllTasksAction;
use App\Application\Actions\Task\FindTaskAction;
use App\Application\Actions\Task\RemoveTaskAction;
use App\Application\Actions\Task\UpdateTaskAction;
use App\Application\Actions\User\AddUserAction;
use App\Application\Actions\User\LoginUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use Slim\Middleware\Authentication\JwtAuthentication;
use App\Application\Settings\SettingsInterface;


return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/api/users', function (Group $group) {
        $group->post('/login', LoginUserAction::class);
        $group->post('', AddUserAction::class);
    });

    $jwtSettings = ($app->getContainer()->get(SettingsInterface::class))->get('jwt');

    if(isset($_ENV['test']) && $_ENV['test']) {
        $jwtSettings['secure'] = false;
    }

    $jwtMiddleware = new JwtAuthentication($app->getContainer(), $jwtSettings);
    $app->group('/api/tasks', function(Group $group) {
        $group->get('', FindAllTasksAction::class);
        $group->post('', AddTaskAction::class);
        $group->put('/{id}', UpdateTaskAction::class);
        $group->delete('/{id}', RemoveTaskAction::class);
        $group->get('/{id}', FindTaskAction::class);
        $group->post('/{id}/complete', CompleteTaskAction::class);
    })->addMiddleware($jwtMiddleware);
};
