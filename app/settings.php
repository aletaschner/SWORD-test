<?php
declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => false,
                'logErrorDetails'     => false,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'jwt' => [
                    'secret' => $_ENV['JWT_SECRET'] ?? 'AIzaSyD3_oKGMD1Hbj8qEt1rSwAQWZ5HslgmCD4',
                    'algorithm' => ['HS256'],
                ],
                'database' => [
                    'driver' => 'mysql',
                    'host' => $_ENV['DB_HOST'] ?? '',
                    'username' => $_ENV['DB_USERNAME'] ?? '',
                    'database' => $_ENV['DB_NAME'] ?? '',
                    'password' => $_ENV['DB_PASSWORD'] ?? '',
                ],
                'encryption' => [
                    'key' => $_ENV['ENCRYPTION_KEY'] ?? '',
                    'algorithm' => $_ENV['ENCRYPTION_ALGORITHM'] ?? 'aes-256-cbc',
                    'iv' => $_ENV['ENCRYPTION_IV'] ?? 'VuIxSKQMkANvvCplvRAu2A=='
                ],
                'message_broker' => [
                    'queue' => $_ENV['MESSAGE_BROKER_QUEUE'] ?? '',
                    'host' => $_ENV['MESSAGE_BROKER_HOST'] ?? '',
                    'port' => $_ENV['MESSAGE_BROKER_PORT'] ?? '',
                    'user' => $_ENV['MESSAGE_BROKER_USER'] ?? '',
                    'password' => $_ENV['MESSAGE_BROKER_PASSWORD'] ?? ''
                ]
            ]);
        }
    ]);
};
