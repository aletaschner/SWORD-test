<?php

declare(strict_types=1);

namespace App\Infrastructure;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PHPUnit\Exception;

class MessageBroker
{
    /** @var AMQPStreamConnection $connection */
    protected $connection;
    /** @var AMQPChannel $channel */
    protected $channel;
    /** @var string $queue */
    protected string $queue;

    public function __construct($queue, $host = 'rabbitmq', $port = 5672, $username = 'user', $password = 'bitnami')
    {
        try {
            $this->queue = $queue;
            $this->setConnection(new AMQPStreamConnection($host, $port, $username, $password));
        } catch (\Exception $e) {
            // There has been an error connecting to RabbitMq
            // Retries can be made but since this is a test, no need to over engineer this
        }
    }

    /**
     * @param AMQPStreamConnection $connection
     * @throws \PhpAmqpLib\Exception\AMQPTimeoutException
     * @return self
     */
    public function setConnection(AMQPStreamConnection $connection) : self
    {
        $this->connection = $connection;
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare($this->queue, false, true, false, false);
        return $this;
    }

    /**
     * @param string $message
     * @return self
     */
    public function notify(string $message) : self
    {
        try {
            $msg = new AMQPMessage($message, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_NON_PERSISTENT]);
            $this->channel->basic_publish($msg, '', $this->queue);
        } catch (\Exception $e) {
            // log exception information
        }

        return $this;
    }

    /**
     * Ends connection
     * @throws \PhpAmqpLib\Exception\AMQPTimeoutException
     */
    public function __destruct()
    {
        if ($this->channel) {
            $this->channel->close();
            $this->connection->close();
        }
    }
}
