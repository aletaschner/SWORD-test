<?php
declare(strict_types=1);

namespace App\Infrastructure;

class DataEncryptor 
{
    protected string $key;
    protected string $algorithm;
    protected string $iv;

    /**
     * @param string $key
     * @param string $algorithm
     * @param string $iv
     */
    public function __construct(string $key, string $algorithm, string $iv) 
    {
        $this->key = $key;
        $this->algorithm = $algorithm;
        $this->iv = base64_decode($iv);
    }

    /**
     * @param string $data
     * @return string
     */
    public function encrypt(string $data)
    {
        return openssl_encrypt($data, $this->algorithm, $this->key, 0, $this->iv);
    }

    /**
     * @param string $data
     * @return string
     */
    public function decrypt(string $data)
    {
        return openssl_decrypt($data, $this->algorithm, $this->key, 0, $this->iv);
    }
}