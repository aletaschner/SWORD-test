<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Infrastructure\Persistence\BaseRepository;
use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;

class DatabaseUserRepository extends BaseRepository implements UserRepository
{
    /**
     * @param string $username
     * @return User
     * @throws UserNotFoundException
     */
    public function findUserOfUsername(string $username): User
    {
        $query = 'SELECT * FROM users WHERE username = ?';
        $prepared = $this->pdo->prepare($query);
        $prepared->bindParam(1, $username, \PDO::PARAM_STR);
        $prepared->execute();
        $user = $prepared->fetch() ?? null;

        if (!$user) {
            throw new UserNotFoundException();
        }

        return new User((int)$user['id'], $user['username'], $user['password'], $user['type']);
    }

    public function addUser(User $user): int
    {
        $query = 'INSERT INTO users (username, type, password) VALUES (:username, :type, :password)';
        $prepared = $this->pdo->prepare($query);
        $prepared->bindValue(':username', $user->getUsername());
        $prepared->bindValue(':type', $user->getType());
        $prepared->bindValue(':password', $user->getPassword());
        $prepared->execute();

        return (int) $this->pdo->lastInsertId();
    }
}
