<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;
use App\Infrastructure\DataEncryptor;

abstract class BaseRepository 
{
    protected \PDO $pdo;

    protected DataEncryptor $dataEncryptor;

    public function __construct(\PDO $pdo, DataEncryptor $dataEncryptor)
    {
        $this->pdo = $pdo;
        $this->dataEncryptor = $dataEncryptor;
    }
}