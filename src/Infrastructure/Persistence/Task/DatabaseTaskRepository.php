<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Task;

use App\Infrastructure\Persistence\BaseRepository;
use App\Domain\Task\Task;
use App\Domain\User\User;
use App\Domain\Task\TaskNotFoundException;
use App\Domain\Task\TaskRepository;

class DatabaseTaskRepository extends BaseRepository implements TaskRepository
{
    /**
     * @param int $userId
     * @param string $userType
     * @return Task[]
     */
    public function findTasksAllowedForUser(int $userId, string $userType): array
    {
        $query = 'SELECT * FROM tasks';

        if ($userType === User::USER_TYPE_TECHNICIAN) {
            $query .= ' WHERE user_id = :id';
        }

        $prepared = $this->pdo->prepare($query);

        if ($userType === User::USER_TYPE_TECHNICIAN) {
            $prepared->bindParam(':id', $userId, \PDO::PARAM_INT);
        }

        $prepared->execute();
        $tasks = $prepared->fetchAll();
        if (count($tasks) > 0) {
            $tasks = $this->transformTasks($tasks);
        }

        return $tasks;
    }

    /**
     * Decrypts the summaries
     * @param array $tasks
     * @return Task[]
     */
    protected function transformTasks(array $tasks): array
    {
        $decryptor = $this->dataEncryptor;
        return array_map(
            function ($item) use ($decryptor) {
                $completedAt = (isset($item['completed_at']) && $item['completed_at'])
                    ? new \DateTime($item['completed_at'])
                    : null;
                return new Task(
                    (int)$item['id'],
                    (int)$item['user_id'],
                    $decryptor->decrypt($item['summary']),
                    $completedAt
                );
            },
            $tasks
        );
    }

    /**
     * @param Task $task
     * @return int The ID that was added
     */
    public function addTask(Task $task): int
    {
        $query = 'INSERT INTO tasks (user_id, summary, completed_at) VALUES (:user_id, :summary, :completed_at)';
        $prepared = $this->pdo->prepare($query);

        $prepared->bindValue(':user_id', $task->getUserId(), \PDO::PARAM_INT);
        $prepared->bindValue(':summary', $this->dataEncryptor->encrypt($task->getSummary()), \PDO::PARAM_STR);
        $prepared->bindValue(
            ':completed_at',
            ($task->getCompletedAt()) ? $task->getCompletedAt()->format('Y-m-d H:i:s') : null
        );
        $prepared->execute();

        return (int)$this->pdo->lastInsertId();
    }

    /**
     * @param int $taskId
     * @param Task $task
     * @return bool
     * @throws TaskNotFoundException
     */
    public function updateTask(int $taskId, Task $task): bool
    {
        $query = 'UPDATE tasks SET summary = :summary, completed_at = :completed_at WHERE id = :id';
        $prepared = $this->pdo->prepare($query);
        $prepared->bindValue(':id', $taskId, \PDO::PARAM_INT);
        $prepared->bindValue(':summary', $this->dataEncryptor->encrypt($task->getSummary()), \PDO::PARAM_STR);
        $prepared->bindValue(
            ':completed_at',
            ($task->getCompletedAt()) ? $task->getCompletedAt()->format('Y-m-d H:i:s') : null
        );

        return $prepared->execute();
    }

    /**
     * @param int $taskId
     * @return bool
     * @throws TaskNotFoundException
     */
    public function removeTask(int $taskId): bool
    {
        $query = 'DELETE FROM tasks WHERE id = :id';
        $prepared = $this->pdo->prepare($query);
        $prepared->bindParam(':id', $taskId, \PDO::PARAM_INT);

        return $prepared->execute();
    }

    /**
     * @param int $taskId
     * @return Task
     * @throws TaskNotFoundException
     */
    public function findTaskById(int $taskId): Task
    {
        $query = 'SELECT * FROM tasks WHERE id = :id';
        $prepared = $this->pdo->prepare($query);
        $prepared->bindParam(':id', $taskId, \PDO::PARAM_INT);
        $prepared->execute();
        $task = $prepared->fetch() ?? null;
        if (!$task) {
            throw new TaskNotFoundException();
        }

        $completedAt = (isset($task['completed_at']) && $task['completed_at'])
            ? new \DateTime($task['completed_at'])
            : null;
        return new Task(
            (int)$task['id'],
            (int)$task['user_id'],
            $this->dataEncryptor->decrypt($task['summary']),
            $completedAt
        );
    }
}
