<?php
declare(strict_types=1);

namespace App\Infrastructure;

use Firebase\JWT\JWT;

class JwtGenerator 
{
    protected string $secret;
    protected string $algorithm;

    public function __construct(string $secret, string $algorithm) 
    {
        $this->secret = $secret;
        $this->algorithm = $algorithm;
    }

    /**
     * @param array $data
     * @return string
     */
    public function generate(array $data)
    {
        return JWT::encode($data, $this->secret, $this->algorithm);
    }
}