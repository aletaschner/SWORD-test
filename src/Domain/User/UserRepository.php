<?php
declare(strict_types=1);

namespace App\Domain\User;

interface UserRepository
{
    /**
     * @param string $username
     * @return User
     * @throws UserNotFoundException
     */
    public function findUserOfUsername(string $username) : User;

    /**
     * @param User $user
     * @return int
     */
    public function addUser(User $user) : int;
}
