<?php
declare(strict_types=1);

namespace App\Domain\User;

use JsonSerializable;

class User implements JsonSerializable
{

    const USER_TYPE_TECHNICIAN = 'technician';
    const USER_TYPE_MANAGER = 'manager';

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @param int|null  $id
     * @param string    $username
     * @param string    $password
     * @param string    $type
     */
    public function __construct(?int $id, string $username, string $password, string $type)
    {
        if($type !== self::USER_TYPE_TECHNICIAN && $type !== self::USER_TYPE_MANAGER) {
            throw new \Exception('Invalid User Type');
        }

        $this->id = $id;
        $this->username = strtolower($username);
        $this->setPassword($password);
        $this->type = $type;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return $this
     */
    protected function setPassword(string $password) : self 
    {
        $this->password = $password;
        $info = password_get_info($password);
        
        // Considering there's no algorithm for the password, it is not hashed
        if (!$info['algo']) {
            $this->password = password_hash($password, PASSWORD_BCRYPT);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'type' => $this->type,
        ];
    }

    /**
     * @param string $password
     * @return bool
     */
    public function isPasswordCorrect(string $password) : bool
    {
        return password_verify($password, $this->getPassword());
    }

    /**
     * @param string $password
     * @return bool
     * @throws IncorrectPasswordException
     */
    public function verifyPasswordOrFail(string $password) : bool
    {
        if (!$this->isPasswordCorrect($password)) {
            throw new IncorrectPasswordException();
        }

        return true;
    }
}
