<?php
declare(strict_types=1);

namespace App\Domain\User;

class IncorrectPasswordException extends UserNotFoundException
{
    // same message as user not found to avoid sensitive information
}
