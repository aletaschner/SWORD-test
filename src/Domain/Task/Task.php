<?php
declare(strict_types=1);

namespace App\Domain\Task;

use JsonSerializable;

class Task implements JsonSerializable
{

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $summary;

    /**
     * @var \DateTime|null
     */
    private $completedAt;

    /**
     * @param int|null          $id
     * @param int               $userId
     * @param string            $summary
     * @param \DateTime|null     $completedAt
     */
    public function __construct(?int $id, int $userId, string $summary, ?\DateTime $completedAt = null)
    {

        $this->id = $id;
        $this->userId = $userId;
        $this->summary = $summary;
        $this->completedAt = $completedAt;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId(int $id) : self 
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return self
     */
    public function setUserId(int $userId) : self 
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return self
     */
    public function setSummary(string $summary) : self 
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCompletedAt() : ?\DateTime 
    {
        return $this->completedAt;
    }

    /**
     * @param \DateTime $completedAt
     * @return self
     */
    public function setCompletedAt(\DateTime $completedAt) : self 
    {
        $this->completedAt = $completedAt;
        return $this;
    }

    /**
     * Sets completed at as current date and time
     * @return self
     */
    public function markAsCompleted() : self
    {
        $this->setCompletedAt(new \DateTime);
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'user_id' => $this->userId,
            'summary' => $this->summary,
            'completed_at' => ($this->completedAt) ? $this->completedAt->format(\DateTime::W3C) : null
        ];
    }

}
