<?php
declare(strict_types=1);

namespace App\Domain\Task;

use Slim\Exception\HttpUnauthorizedException;

class InvalidTaskPermission extends HttpUnauthorizedException
{
    public $message = 'You are not authorized to do this action.';
}
