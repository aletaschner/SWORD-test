<?php
declare(strict_types=1);

namespace App\Domain\Task;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;

class InvalidTaskDataException extends HttpBadRequestException
{

}
