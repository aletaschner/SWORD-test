<?php
declare(strict_types=1);

namespace App\Domain\Task;

use App\Domain\User\User;

interface TaskRepository
{
    /**
     * @param int $userId
     * @param string $userType
     * @return Task[]
     */
    public function findTasksAllowedForUser(int $userId, string $userType) : array;

    /**
     * @param Task $task
     * @return int The ID that was added
     */
    public function addTask(Task $task) : int;

    /**
     * @param int $taskId
     * @param Task $task
     * @return bool
     * @throws TaskNotFoundException
     */
    public function updateTask(int $taskId, Task $task) : bool;

    /**
     * @param int $taskId
     * @return bool
     * @throws TaskNotFoundException
     */
    public function removeTask(int $taskId) : bool;

    /**
     * @param int $taskId
     * @return Task
     * @throws TaskNotFoundException
     */
    public function findTaskById(int $taskId) : Task;
}
