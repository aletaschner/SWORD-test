<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Actions\Action;
use App\Domain\User\UserRepository;
use Psr\Log\LoggerInterface;
use App\Infrastructure\JwtGenerator;

abstract class UserAction extends Action
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    protected $jwtGenerator;
    /**
     * @param LoggerInterface $logger
     * @param UserRepository $userRepository
     * @param JwtGenerator $jwtGenerator
     */
    public function __construct(LoggerInterface $logger,
                                UserRepository $userRepository,
                                JwtGenerator $jwtGenerator
    ) {
        parent::__construct($logger);
        $this->userRepository = $userRepository;
        $this->jwtGenerator = $jwtGenerator;
    }
}
