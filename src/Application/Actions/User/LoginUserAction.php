<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;

class LoginUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $requestBody = $this->request->getParsedBody();
        
        $username = $requestBody['username'] ?? '';
        $password = $requestBody['password'] ?? '';

        $user = $this->userRepository->findUserOfUsername($username);
        $user->verifyPasswordOrFail($password);

        //$this->logger->info("User of username `" . $user->getUsername() . "` has logged in successfully.");
        return $this->respondWithData($this->jwtGenerator->generate($user->jsonSerialize()));
    }
}
