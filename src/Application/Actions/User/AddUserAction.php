<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Domain\User\User;
use Psr\Http\Message\ResponseInterface as Response;

class AddUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $requestBody = $this->request->getParsedBody();

        $user = new User(
            null,
            $requestBody['username'] ?? '',
            $requestBody['password'] ?? '',
            $requestBody['type'] ?? User::USER_TYPE_TECHNICIAN
        );

        $id = $this->userRepository->addUser($user);

        //$this->logger->info("User of username `" . $user->getUsername() . "` added.");
        return $this->respondWithData($id)->withStatus(201);
    }
}
