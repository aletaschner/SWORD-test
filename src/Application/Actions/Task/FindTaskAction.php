<?php

declare(strict_types=1);

namespace App\Application\Actions\Task;

use App\Domain\Task\InvalidTaskPermission;
use App\Domain\User\User;
use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Task\Task;

class FindTaskAction extends TaskAction
{

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $taskId = (int) $this->resolveArg('id');

        $task = $this->taskRepository->findTaskById($taskId);
        $this->validate($task);

        //$this->logger->info("The task with the id `$taskId` has been viewed.");
        return $this->respondWithData($task)->withStatus(201);
    }

    /**
     * @param Task $task
     * @return self
     */
    protected function validate(Task $task): self
    {
        $userData = $this->request->getAttribute('token');

        if ($userData->id !== $task->getUserId() && $userData->type !== User::USER_TYPE_MANAGER) {
            throw new InvalidTaskPermission($this->request);
        }

        return $this;
    }
}
