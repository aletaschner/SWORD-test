<?php

declare(strict_types=1);

namespace App\Application\Actions\Task;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Task\Task;
use App\Domain\Task\InvalidTaskDataException;
use Slim\Exception\HttpUnauthorizedException;

class FindAllTasksAction extends TaskAction
{

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $userData = $this->request->getAttribute('token');

        $tasks = $this->taskRepository->findTasksAllowedForUser($userData->id, $userData->type);

//        $logMessage = \sprintf('The user %s (%s) has viewed the tasks', $userData['username'], $userData['type']);
//        $this->logger->info($logMessage);
        return $this->respondWithData(($tasks));
    }
}
