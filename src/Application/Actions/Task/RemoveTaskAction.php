<?php

declare(strict_types=1);

namespace App\Application\Actions\Task;

use App\Domain\Task\InvalidTaskPermission;
use App\Domain\User\User;
use Psr\Http\Message\ResponseInterface as Response;

class RemoveTaskAction extends TaskAction
{

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $taskId = (int) $this->resolveArg('id');

        $this->taskRepository->findTaskById($taskId);
        $this->validate();

        $this->taskRepository->removeTask($taskId);
        //$this->logger->info("The task with the id `$taskId` has been deleted.");
        return $this->respondWithData(true)->withStatus(204);
    }

    /**
     * @return self
     */
    protected function validate(): self
    {
        $userData = $this->request->getAttribute('token');

        if ($userData->type !== User::USER_TYPE_MANAGER) {
            throw new InvalidTaskPermission($this->request);
        }

        return $this;
    }
}
