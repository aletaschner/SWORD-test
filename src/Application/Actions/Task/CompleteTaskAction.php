<?php

declare(strict_types=1);

namespace App\Application\Actions\Task;

use App\Domain\Task\InvalidTaskPermission;
use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Task\Task;

class CompleteTaskAction extends TaskAction
{

    protected \StdClass $userData;

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $taskId = (int)$this->resolveArg('id');

        $task = $this->taskRepository->findTaskById($taskId);
        $this->validate($task);
        $task->markAsCompleted();
        $this->taskRepository->updateTask($taskId, $task);
        $message = 'The technician %s performed the task %s on date %s';
        $this->messageBroker->notify(
            sprintf(
                $message,
                $this->userData->username,
                $task->getId(),
                $task->getCompletedAt()->format('Y-m-d H:i:s')
            )
        );

        //$this->logger->info("The task with the id `$taskId` has been completed.");
        return $this->respondWithData(true)->withStatus(200);
    }

    /**
     * @param Task $task
     * @return self
     */
    protected function validate(Task $task): self
    {
        $this->userData = $this->request->getAttribute('token');

        if ($this->userData->id !== $task->getUserId()) {
            throw new InvalidTaskPermission($this->request);
        }

        return $this;
    }
}
