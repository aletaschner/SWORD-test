<?php

declare(strict_types=1);

namespace App\Application\Actions\Task;

use App\Domain\Task\InvalidTaskPermission;
use App\Domain\User\User;
use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Task\Task;
use App\Domain\Task\InvalidTaskDataException;
use Slim\Exception\HttpUnauthorizedException;

class AddTaskAction extends TaskAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->requestBody = (array)$this->request->getParsedBody();
        $task = $this->validate();
        $taskId = (int) $this->taskRepository->addTask($task);

        //$this->logger->info("A new task was added with the id `$taskId`.");
        return $this->respondWithData(\json_encode($taskId))->withStatus(201);
    }

    /**
     * @return Task validated task
     */
    protected function validate(): Task
    {
        $userData = $this->request->getAttribute('token');
        $task = new Task(
            null,
            $userData->id,
            $this->requestBody['summary'] ?? '',
            (isset($this->requestBody['completed_at']) && $this->requestBody['completed_at'])
                ? new \DateTime($this->requestBody['completed_at']) : null
        );

        if ($userData->type !== User::USER_TYPE_TECHNICIAN) {
            throw new InvalidTaskPermission($this->request);
        }

        if (!$task->getSummary()) {
            throw new InvalidTaskDataException($this->request, 'You must provide a summary.');
        }

        if (\strlen($task->getSummary()) > 2500) {
            throw new InvalidTaskDataException($this->request, 'The summary can not exceed 2500 characters.');
        }

        return $task;
    }
}
