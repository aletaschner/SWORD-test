<?php
declare(strict_types=1);

namespace App\Application\Actions\Task;

use App\Application\Actions\Action;
use App\Domain\Task\TaskRepository;
use Psr\Log\LoggerInterface;
use App\Infrastructure\MessageBroker;

abstract class TaskAction extends Action
{
    /**
     * @var TaskRepository
     */
    protected $taskRepository;

    /** 
     * @var array $requestBody 
     */
    protected array $requestBody;

    protected MessageBroker $messageBroker;

    /**
     * @param LoggerInterface $logger
     * @param TaskRepository $taskRepository
     */
    public function __construct(LoggerInterface $logger,
                                TaskRepository $taskRepository,
                                MessageBroker $messageBroker
    ) {
        parent::__construct($logger);
        $this->taskRepository = $taskRepository;
        $this->messageBroker = $messageBroker;
    }
}
