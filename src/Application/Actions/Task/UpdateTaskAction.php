<?php

declare(strict_types=1);

namespace App\Application\Actions\Task;

use App\Domain\Task\InvalidTaskPermission;
use App\Domain\User\User;
use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Task\Task;
use App\Domain\Task\InvalidTaskDataException;

class UpdateTaskAction extends TaskAction
{
    /** @var \StdClass $userData */
    protected \StdClass $userData;

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->requestBody = (array)$this->request->getParsedBody();
        $taskId = (int)$this->resolveArg('id');

        $databaseTask = $this->taskRepository->findTaskById($taskId);
        $task = $this->validate($taskId, $databaseTask);

        if ($task->getCompletedAt()) {
            $message = 'The technician %s performed the task %s on date %s';
            $this->messageBroker->notify(
                sprintf(
                    $message,
                    $this->userData->username,
                    $task->getId(),
                    $task->getCompletedAt()->format('Y-m-d H:i:s')
                )
            );
        }

        $this->taskRepository->updateTask($taskId, $task);
        // $this->logger->info("The task with `$taskId` has been updated.");
        return $this->respondWithData(true)->withStatus(204);
    }

    /**
     * @param int $taskId
     * @param Task $databaseTask
     * @return Task validated task
     */
    protected function validate(int $taskId, Task $databaseTask): Task
    {
        $this->userData = $this->request->getAttribute('token');
        $task = new Task(
            $taskId,
            $this->userData->id,
            $this->requestBody['summary'] ?? '',
            (isset($this->requestBody['completed_at']) && $this->requestBody['completed_at'])
                ? new \DateTime($this->requestBody['completed_at'])
                : null
        );

        if ($task->getUserId() !== $databaseTask->getUserId()) {
            throw new InvalidTaskPermission($this->request);
        }

        if ($this->userData->type !== User::USER_TYPE_TECHNICIAN) {
            throw new InvalidTaskPermission($this->request);
        }

        if (!$task->getSummary()) {
            throw new InvalidTaskDataException($this->request, 'You must provide a summary.');
        }

        if (strlen($task->getSummary()) > 2500) {
            throw new InvalidTaskDataException($this->request, 'The summary can not exceed 2500 characters.');
        }

        return $task;
    }
}
