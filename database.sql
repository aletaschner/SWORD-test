CREATE SCHEMA taskapp;
USE taskapp;

CREATE TABLE `tasks`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT,
    `user_id`      int(11) NOT NULL,
    `summary`      text     DEFAULT NULL,
    `completed_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `users`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(200) NOT NULL,
    `type`     enum('technician','manager') NOT NULL,
    `password` varchar(250) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `users_username_uindex` (`username`)
);

INSERT INTO `users` (`id`, `username`, `type`, `password`)
VALUES (1, 'bill.gates', 'technician', '$2y$10$Lo1ECG3AwfxoW4ycnJmMo.pBzg3x8VjKXEB2x3l/BcMScmzmQl0S.'),
       (2, 'steve.jobs', 'manager', '$2y$10$NNLb.iGRteMpiEWE/MeXGOGhXLwmyh5q9JIjDRhrUYTZyb29RD6Dq'),
       (3, 'erlich.bachman', 'technician', '$2y$10$QRGPkgpnrvmfgvS4o2A3O.O1F7uhgSTpL847CRdRAY1tJ7O5QCMxu');

